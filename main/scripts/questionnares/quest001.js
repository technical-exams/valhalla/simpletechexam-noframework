var quest001 = (() => {
    var menu;
    var loaded = false;
    var init = (data) => {
        menu = data;
        process.createEvents();
        process.createElements();
    }

    var process = {
        createEvents: () => {
            if(!loaded){
                loaded = true;
                $(document).on('keyup', '.q001-left', (events) => {
                    try{
                        var _val = process.reverseValue($(events.target).val());
                        $('.q001-right').val(_val)
                    }catch(err){}
                });
                $(document).on('keyup', '.q001-right', (events) => {
                    try{
                        var _val = process.reverseValue($(events.target).val());
                        $('.q001-left').val(_val)
                    }catch(err){}
                });
            }
        },
        createElements: () => {
            $('.bodycontent').empty();
            var parentDiv = cmnConstants.commonFormats.addParentDiv();

            cmnConstants.commonFormats.addTitle(parentDiv, menu);
            cmnConstants.commonFormats.addDescription(parentDiv, menu);
            process.addMultilines(parentDiv);

            $('.bodycontent').append(parentDiv);
        },
        addMultilines: (parentDiv) => {
            var divisor = document.createElement('div');
            var textLeft = document.createElement('textarea');
            var textRight = document.createElement('textarea');

            divisor.setAttribute('class', 'row col-md-12 demargined');
            textLeft.setAttribute('class', 'form-control col-md-6 q001-left')
            textRight.setAttribute('class', 'form-control col-md-6 q001-right')

            $(divisor).append(textLeft);
            $(divisor).append(textRight);
            $(parentDiv).append(divisor);
        },
        reverseValue: (str) => {
            var comma = btoa(new Date().toISOString());
            var splitVal = str.replace(/,/g, comma).replace(/\n/g, ' ').split(" ");
            splitVal = splitVal.reverse();
            return splitVal.toString().replace(/,/g, ' ').replace(new RegExp(comma, 'g'), ',');
        }
    }

    return {
        init: init
    }
})();