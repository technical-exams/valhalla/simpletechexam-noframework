var mainProcess = (() => {
    var menus = cmnConstants.getMenus();
    var init = {
        createMenus: () => {
            init.createEvents();
            $('.sidemenu').empty();

            var parentDiv = document.createElement('div');
            parentDiv.setAttribute('class', 'list-group snapper');
            $.each(menus, (i,v) => {
                var list = document.createElement('a');
                list.setAttribute('href', '#');
                list.setAttribute('class', 'list-group-item list-group-item-action decolored sidemenu-list');
                list.setAttribute('data-order', v.order);
                $(list).text(v.order.toString() + '. ' + v.title);
                $(parentDiv).append(list);
            });
            $('.sidemenu').append(parentDiv);
        },
        createEvents: () => {
            $(document).on('click', '.sidemenu-list', (events) => {
                try{
                    $('.bodycontent').empty();
                    var selectedOrder = events.target.dataset.order;
                    var findMenu = menus.find( f => { return f.order.toString() === selectedOrder.toString();});
                    
                    if(findMenu) findMenu.init(findMenu);
                }catch(err){ alert("Source code has a problem please check main.js->init->createEvents"); }
            });
        }
    }

    return {
        init: init
    }
})();

mainProcess.init.createMenus();