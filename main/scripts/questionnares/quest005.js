var quest005 = (() => {
    var menu, arrList;
    var loaded = false;

    var init = (data) => {
        menu = data;
        arrList = [];

        process.createEvents();
        process.createElements();
    }

    var process = {
        createEvents: () => {
            if(!loaded){
                loaded = true;
                $(document).on('click', '.q005-btnOk', (events) => { process.addNewItem(); });
                $(document).on('keyup', '.q005-input', (events) => { if(events.keyCode === 13) process.addNewItem() });
                $(document).on('click', '.q005-btnCancel', (events) => {
                    arrList = [];
                    $('.q005-input').val('');
                    process.updateDisplay();
                });
                $(document).on('click', '.q005-left-list', (events) => {
                    var pointer = events.target.dataset.pointer;
                    arrList.splice(parseInt(pointer), 1);
                    process.updateDisplay();
                });
            }
        },
        createElements: () => {
            $('.bodycontent').empty();
            var parentDiv = cmnConstants.commonFormats.addParentDiv();

            cmnConstants.commonFormats.addTitle(parentDiv, menu);
            cmnConstants.commonFormats.addDescription(parentDiv, menu);
            process.addInputs(parentDiv);
            cmnConstants.commonFormats.interval(parentDiv);
            process.addList(parentDiv);

            $('.bodycontent').append(parentDiv);
        },
        addNewItem: () => {
            var value = parseInt($('.q005-input').val());
            $('.q005-input').val('');
            arrList.push(value);
            process.updateDisplay();
        },
        addInputs: (parentDiv) => {
            var divisor = document.createElement('div');
            var inputBox = document.createElement('input');
            var dataPush = document.createElement('button');
            var dataClear = document.createElement('button');

            divisor.setAttribute('class', 'row col-md-6 demargined');
            inputBox.setAttribute('class', 'form-control col-md-6 q005-input');
            inputBox.setAttribute('placeholder', 'Input a number here!');
            inputBox.setAttribute('type', 'number');

            dataPush.setAttribute('class', 'btn col-md-3 btn-primary q005-btnOk');
            $(dataPush).text('Add');
            dataClear.setAttribute('class', 'btn col-md-3 btn-danger q005-btnCancel');
            $(dataClear).text('Clear');

            $(divisor).append(inputBox);
            $(divisor).append(dataPush);
            $(divisor).append(dataClear);
            $(parentDiv).append(divisor);

            divisor = document.createElement('div');
            var identifier = document.createElement('span');

            divisor.setAttribute('class', 'row col-md-6 demargined');
            identifier.setAttribute('class', 'input-group-text q005-identifier decolored');

            $(identifier).text('No');

            $(divisor).append(identifier);
            $(parentDiv).append(divisor);
        },
        addList: (parentDiv) => {
            var divisor = document.createElement('div');
            var listLeft = document.createElement('div');

            divisor.setAttribute('class', 'row col-md-12 demargined');
            listLeft.setAttribute('class', 'list-group snapper col-md-6 q005-left');

            $(divisor).append(listLeft);
            $(parentDiv).append(divisor);
        },
        updateDisplay: () =>{
            $('.q005-left').empty();
            $('.q005-identifier').text('No').removeClass('have-8');

            for(var i=0; i<arrList.length; i++){
                var list = document.createElement('a');

                list.setAttribute('href', '#');
                list.setAttribute('class', 'list-group-item list-group-item-action decolored q005-left-list');
                list.setAttribute('data-pointer', i);
                $(list).text(arrList[i]);

                $('.q005-left').append(list);
            }

            var have8Flag = false;
            for(var i=0; i<arrList.length; i++){
                var combo = 0;
                for(var x=i+1; x<arrList.length; x++){
                    combo = arrList[i] + arrList[x];
                    if(combo === 8) have8Flag = true;
                    if(have8Flag) break;
                }
                if(have8Flag) break;
            }

            if(have8Flag) $('.q005-identifier').text('Yes').addClass('have-8');
        }
    }

    return {
        init: init
    }
})();