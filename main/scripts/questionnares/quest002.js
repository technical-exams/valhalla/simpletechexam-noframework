var quest002 = (() => {
    var menu;
    var loaded = false;
    var init = (data) => {
        menu = data;
        process.createEvents();
        process.createElements();
    }

    var process = {
        createEvents: () => {
            if(!loaded){
                loaded = true;
                $(document).on('keyup change', '.q002-left', (events) => {
                    var _val = $(events.target).val();
                    if(_val){
                        try{
                            $('.q002-right').val(process.generateSequence(parseInt(_val)));
                        }catch(err){ $('.q002-right').val('Something wrong with the input') }
                    }else $('.q002-right').val('')
                });
            }
        },
        createElements: () => {
            $('.bodycontent').empty();
            var parentDiv = cmnConstants.commonFormats.addParentDiv();

            cmnConstants.commonFormats.addTitle(parentDiv, menu);
            cmnConstants.commonFormats.addDescription(parentDiv, menu);
            process.addInputBoxes(parentDiv);

            $('.bodycontent').append(parentDiv);
        },
        addInputBoxes: (parentDiv) => {
            var divisor = document.createElement('div');
            var textLeft = document.createElement('input');
            var textRight = document.createElement('input');

            divisor.setAttribute('class', 'row col-md-12 demargined');
            textLeft.setAttribute('class', 'form-control col-md-6 q002-left');
            textLeft.setAttribute('placeholder', 'Input a number here!');
            textLeft.setAttribute('type', 'number');
            textRight.setAttribute('class', 'form-control col-md-6 q002-right');
            textRight.setAttribute('disabled', 'disabled')

            $(divisor).append(textLeft);
            $(divisor).append(textRight);
            $(parentDiv).append(divisor);
        },
        generateSequence: (val) => {
            if(val === 0) return 0;
            else if(val === 1 || val === -1) return 1;
            
            var isNegative = val < 0 ? true: false;
            var absoluteVal = isNegative ? process.absoluteValue(val): val;

            var newData = [0,1];
            for(var i=2; i<=absoluteVal; i++){
                var _compute = process.absoluteValue(newData[i-1]) + process.absoluteValue(newData[i-2]);
                if(isNegative) _compute = i % 2 === 0 ? _compute*(-1): _compute;
                newData.push(_compute);
            }

            return newData[newData.length - 1];
        },
        absoluteValue: (num) => {
            return Math.abs(num);
        }
    }

    return {
        init: init
    }
})();