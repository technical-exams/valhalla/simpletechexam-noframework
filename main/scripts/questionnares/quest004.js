var quest004 = (() => {
    var menu, arrList;
    var loaded = false;

    var init = (data) => {
        menu = data;
        arrList = [];
        process.createEvents();
        process.createElements();
    }

    var process = {
        createEvents: () => {
            if(!loaded){
                loaded = true;
                $(document).on('keyup', '.q004-left', (events) =>{
                    try{
                        var value = $(events.target).val().trim().replace(/ /g, '');
                        var search = process.search(value);
                        if(search) $('.q004-right').val(search.char);
                        else $('.q004-right').val('null');
                    }catch(err){ $('.q004-right').val('null')}
                });
            }
        },
        createElements: () => {
            $('.bodycontent').empty();
            var parentDiv = cmnConstants.commonFormats.addParentDiv();

            cmnConstants.commonFormats.addTitle(parentDiv, menu);
            cmnConstants.commonFormats.addDescription(parentDiv, menu);
            process.addInputBoxes(parentDiv);

            $('.bodycontent').append(parentDiv);
        },
        addInputBoxes: (parentDiv) => {
            var divisor = document.createElement('div');
            var textLeft = document.createElement('input');
            var textRight = document.createElement('input');

            divisor.setAttribute('class', 'row col-md-12 demargined');
            textLeft.setAttribute('class', 'form-control col-md-6 q004-left');
            textLeft.setAttribute('placeholder', 'Input a string here!');
            textLeft.setAttribute('type', 'text');
            textRight.setAttribute('class', 'form-control col-md-6 q004-right');
            textRight.setAttribute('disabled', 'disabled');

            $(divisor).append(textLeft);
            $(divisor).append(textRight);
            $(parentDiv).append(divisor);
        },
        search: (str) => {
            arrList = [];

            var firstRepetition = false;
            for(var i=0; i<str.length; i++){
                var substr = str.substr(i,1);
                var index = arrList.findIndex(f => { return f.char === substr });

                if(index > -1){
                    if(!firstRepetition){
                        firstRepetition = true;
                        arrList[index].index = true;
                    }
                    arrList[index].count++;
                }
                else arrList.push({char:substr,count: 1, index: false});
            }

            return arrList.find(f => { return f.index === true});
        }
    }

    return {
        init: init
    }
})();