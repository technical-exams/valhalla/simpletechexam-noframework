var cmnConstants = (() => {
    const menus = [
        {
            title: 'Word Reverse',
            description: 'Write a function that asks the user for a string containing multiple words. Print back to the user the same string, except with the words in backwards order.',
            order: 1,
            init: (data) => {return quest001.init(data)}
        },
        {
            title: 'Fibonacci Sequencer',
            description: 'Write a program that asks the user how many Fibonacci numbers to generate and then generates them. Take this opportunity to think about how you can use functions. Make sure to ask the user to enter the number of numbers in the sequence to generate.',
            order: 2,
            init: (data) => {return quest002.init(data)}
        },
        {
            title: 'Number Sorter',
            description: 'Create a function that will sort the given array.',
            order: 3,
            init: (data) => {return quest003.init(data)}
        },
        {
            title: 'First Repetition',
            description: 'Create a function that will get the First Recurring Character feeded by string',
            order: 4,
            init: (data) => {return quest004.init(data)}
        },
        {
            title: 'Do I have 8?',
            description: 'Create a function that will check an Array of numbers for all combinations of 2 numbers and tell if any combination is equal to 8.',
            order: 5,
            init: (data) => {return quest005.init(data)}
        },
    ]

    return {
        getMenus: () => { return menus },
        commonFormats: {
            addTitle: (parentDiv, menu) => {
                var title = document.createElement('h2');
                title.setAttribute('class', 'col-md-12');
                $(title).text(menu.title);
                $(parentDiv).append(title);
            },
            addDescription: (parentDiv, menu) => {
                var desc = document.createElement('h5');
                desc.setAttribute('class', 'col-md-12');
                $(desc).text(menu.description);
                $(parentDiv).append(desc);
            },
            addParentDiv: () => {
                var parentDiv = document.createElement('div');
                parentDiv.setAttribute('class', 'row col-md-12 snapper decolored');
                return parentDiv;
            },
            interval: (parentDiv) => {
                var pad = document.createElement('div');
                pad.setAttribute('class', 'col-md-12 padded');
                $(parentDiv).append(pad);
            }
        }
    }
})();