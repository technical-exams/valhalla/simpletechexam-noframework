var quest003 = (() => {
    var menu, arrList, sort_order;
    var loaded = false;

    var init = (data) => {
        menu = data;
        sort_order = 'asc'
        arrList = [];

        process.createEvents();
        process.createElements();
    }

    var process = {
        createEvents: () => {
            if(!loaded){
                loaded = true;
                $(document).on('click', '.btnOk', (events) => { process.addNewItem(); });
                $(document).on('keyup', '.q003-input', (events) => { if(events.keyCode === 13) process.addNewItem() });
                $(document).on('click', '.btnCancel', (events) => {
                    arrList = [];
                    $('.q003-input').val('');
                    process.updateDisplay();
                });
                $(document).on('click', '.q003-left-list', (events) => {
                    var pointer = events.target.dataset.pointer;
                    arrList.splice(parseInt(pointer), 1);
                    process.updateDisplay();
                });
                $(document).on('change', '.q003-sorter', (events) => {
                    var sortBy = $(events.target).prop('checked');
                    if(!sortBy){
                        $('.q003-labelsort').text('Will be sort in ascending order');
                        sort_order = 'asc';
                    }else {
                        $('.q003-labelsort').text('Will be sort in descending order');
                        sort_order = 'desc';
                    }
                    process.updateDisplay();
                });
            }
        },
        createElements: () => {
            $('.bodycontent').empty();
            var parentDiv = cmnConstants.commonFormats.addParentDiv();

            cmnConstants.commonFormats.addTitle(parentDiv, menu);
            cmnConstants.commonFormats.addDescription(parentDiv, menu);
            process.addInputs(parentDiv);
            cmnConstants.commonFormats.interval(parentDiv);
            process.addList(parentDiv);

            $('.bodycontent').append(parentDiv);
        },
        addNewItem: () => {
            var value = parseInt($('.q003-input').val());
            $('.q003-input').val('');
            arrList.push(value);
            process.updateDisplay();
        },
        addInputs: (parentDiv) => {
            var divisor = document.createElement('div');
            var inputBox = document.createElement('input');
            var dataPush = document.createElement('button');
            var dataClear = document.createElement('button');

            divisor.setAttribute('class', 'row col-md-6 demargined');
            inputBox.setAttribute('class', 'form-control col-md-6 q003-input');
            inputBox.setAttribute('placeholder', 'Input a number here!');
            inputBox.setAttribute('type', 'number');

            dataPush.setAttribute('class', 'btn col-md-3 btn-primary btnOk');
            $(dataPush).text('Add');
            dataClear.setAttribute('class', 'btn col-md-3 btn-danger btnCancel');
            $(dataClear).text('Clear');

            $(divisor).append(inputBox);
            $(divisor).append(dataPush);
            $(divisor).append(dataClear);
            $(parentDiv).append(divisor);

            divisor = document.createElement('div');
            var sorter = document.createElement('input');
            var labelsort = document.createElement('span');

            divisor.setAttribute('class', 'row col-md-6 demargined');
            sorter.setAttribute('class', 'input-group-text q003-sorter');
            sorter.setAttribute('type', 'checkbox');
            labelsort.setAttribute('class', 'input-group-text q003-labelsort decolored');

            $(labelsort).text('Will be sort in ascending order');

            $(divisor).append(sorter);
            $(divisor).append(labelsort);
            $(parentDiv).append(divisor);
        },
        addList: (parentDiv) => {
            var divisor = document.createElement('div');
            var listLeft = document.createElement('div');
            var listRight = document.createElement('div');

            divisor.setAttribute('class', 'row col-md-12 demargined');
            listLeft.setAttribute('class', 'list-group snapper col-md-6 q003-left');
            listRight.setAttribute('class', 'list-group snapper col-md-6 q003-right');

            $(divisor).append(listLeft);
            $(divisor).append(listRight);
            $(parentDiv).append(divisor);
        },
        updateDisplay: () =>{
            $('.q003-left').empty();
            $('.q003-right').empty();

            var sorted = [];
            for(var i=0; i<arrList.length; i++){
                sorted.push(arrList[i]);
                var list = document.createElement('a');

                list.setAttribute('href', '#');
                list.setAttribute('class', 'list-group-item list-group-item-action decolored q003-left-list');
                list.setAttribute('data-pointer', i);
                $(list).text(arrList[i]);

                $('.q003-left').append(list);
            }

            if(sort_order === 'asc') sorted.sort((a,b) => a - b);
            else sorted.sort((a,b) => b - a);

            for (var i = 0; i < sorted.length; i++) {
                var list = document.createElement('div');
                list.setAttribute('href', '#');
                list.setAttribute('class', 'list-group-item list-group-item-action decolored');
                $(list).text(sorted[i]);

                $('.q003-right').append(list);
            }
        }
    }

    return {
        init: init
    }
})();