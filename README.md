# SimpleTechExam-NoFramework

### 1. Write a function that asks the user for a string containing multiple words. Print back to the user the same string, except with the words in backwards order. 
- For example, say I type the string: ***This is Valhalla***
- Then I would see the string: ***Valhalla is this***

### 2. Write a program that asks the user how many Fibonacci numbers to generate and then generates them. Take this opportunity to think about how you can use functions. Make sure to ask the user to enter the number of numbers in the sequence to generate.

### 3. Create a function that will sort the given array.
- Sample array : ***[34,7,23,32,5,62]***
- Sample output : ***[5, 7, 23, 32, 34, 62]***

### 4. Create a function that will get the First Recurring Character feeded by string
- Sample inputs and outputs: 
    - ***“ABCA”=> “A”***
    - ***“CABDBA”=> “B”***
    - ***“CBAD” => null***

### 5. Create a function that will check an Array of numbers for all combinations of 2 numbers and tell if any combination is equal to 8.
- Sample arrays:
    - ***[1,2,3,4] Sum = 8 No***
    - ***[4,2,4,1] Sum = 8 Yes***
    - ***[7,2,4,6,7] Sum = 8 Yes***
